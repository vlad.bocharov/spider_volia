from flask import Flask, make_response, request, current_app, jsonify, render_template
from flask.ext.cors import CORS
from functools import update_wrapper
import os, requests
from bs4 import BeautifulSoup


app = Flask(__name__)
# cors = CORS(app)
volia_ref = 'https://my.volia.com'
init_ref = 'https://my.volia.com/kiev/ru/faq'
categories = {'Интернет': 'supportFAQ__block supportFAQ__block_internet',
			  'Телевидение': 'supportFAQ__block supportFAQ__block_tv',
			  'Финансы': 'supportFAQ__block supportFAQ__block_finance',
			  'Другое': 'supportFAQ__block supportFAQ__block_other'}
faq_all = dict()

def json_page(req):
	''' page article to json '''
	soup = BeautifulSoup(r.text, 'lxml')
	# temp test
	return {'title': soup.find_all('div', class_='supportQuestionList__article')[0].contents}

def reload_faq():
	r = requests.get(init_ref)
	soup = BeautifulSoup(r.text, 'lxml')
	for category in categories:
		# print(category)
		category_items = dict()
		category_a = soup.find_all("div", class_=categories[category])[0].find_all('a')
		# print(category_a)
		for item in category_a:
			# print(item['href'])
			# print(' '*4, item.string)
			path = item['href']
			r_ch = requests.get(volia_ref+path)
			soup_ch = BeautifulSoup(r_ch.text, 'lxml')
			article_a = soup_ch.find_all("div", class_='supportQuestionList__article')[0].find_all('a')
			article_tags = dict()
			for article_tag in article_a:
				article_ref = requests.get(volia_ref+article_tag['href'])
				# print(' '*8, article_tag.string, article_ref)
				article_tags[article_tag.string] = json_page(article_ref)
			category_items[item.string] = article_tags
		faq_all[category] = category_items
	# db
	client = MongoClient()
	db = client.volia
	coll=db.volia_faq
	coll.delete_many({})
	coll.insert_one(faq_all)

@app.route("/api/", methods=['GET'])
def index():
	init_ref = 'https://my.volia.com/kiev/ru/faq'
	r = request(init_ref)
	print(res)



if __name__ == '__main__':
	# app.run(host=os.getenv('IP', '0.0.0.0'),port=int(os.getenv('PORT', 8080)))
	
	# test
	r = requests.get('https://my.volia.com/kiev/ru/faq/article/propal-dostup-k-internetu-chto-delat-tehnologiya-docsis;stjsid=1v208y2c78pufc6ie0udv2ug')
	res = json_page(r)
	print(res)
	

